﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarroAtributos
{
    class Carro
    {
        public string nomeDoProprietario;
        public string marca;
        public string corDoCarro;
        public bool temAirBag;
        public int quantidadeDePortas;

        public void Nome()
        {
            while (nomeDoProprietario != "Gabriel")
            {
                Console.WriteLine("Nome invalido!! Por favor digite outro nome: ");
                nomeDoProprietario = Console.ReadLine();
            }
            Console.WriteLine("Nome aceito com sucesso. ");

        }

        public void Marca()
        {
            while (marca != "Honda" && marca != "Hyundai")
            {
                Console.WriteLine("Digite uma marca valida. ");
                marca = Console.ReadLine();
            }
            Console.WriteLine("Ok, marca " + marca + " validada com sucésso. ");

        }

        public void Cor()
        {
            while (corDoCarro != "Azul" && corDoCarro != "Branco")
            {
                Console.WriteLine("Opção invalida: Por favor escolha uma cor existente em nosso sistema: ");
                corDoCarro = Console.ReadLine();
            }
            Console.WriteLine("Parabéns cor definida com sucesso. " + "\nA cor escolhida foi " + corDoCarro + ".");

        }

        public void Airbag(string entradaTemAirBag)
        {
            if (entradaTemAirBag == "Sim")
            {
                temAirBag = true;
            }
            else if (entradaTemAirBag == "Não")
            {
                temAirBag = false;
            }
        }

        public void QuantidadeDePortas()
        {
            while (quantidadeDePortas >= 5)
            {
                Console.WriteLine("Digite um valor valido: "
                + " 2 = [Duas Portas] ou 4 = [Quatro Portas] ");
                quantidadeDePortas = Convert.ToInt32(Console.ReadLine());
            }
        }

        public void Imprimir()
        {
            string descricaoTemAirbag;
            if (temAirBag == true)
            {
                descricaoTemAirbag = "Sim";
            }
            else
            {
                descricaoTemAirbag = "Não";
            }
            Console.WriteLine("O nome do proprietário é " + nomeDoProprietario + ", e a marca do carro é " + marca
                + ".\nA cor do carro é " + corDoCarro + "." + " O carro tem airbag? " + descricaoTemAirbag + " e tem " + quantidadeDePortas + " portas.");
        }

    }

}