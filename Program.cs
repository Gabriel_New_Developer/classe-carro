﻿using System;

namespace CarroAtributos
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro carro = new Carro();

            Console.WriteLine("Digite o nome do proprietário: ");
            carro.nomeDoProprietario = Console.ReadLine();
            carro.Nome();

            Console.WriteLine("Digite a marca: ");
            carro.marca = Console.ReadLine();
            carro.Marca();

            Console.WriteLine("Digite a cor do carro :");
            carro.corDoCarro = Console.ReadLine();
            carro.Cor();

            Console.WriteLine("Tem Airbag? [Sim/Não] ");

            carro.Airbag(Console.ReadLine());

            Console.WriteLine("Digite a quantidade de portas :");
            carro.quantidadeDePortas = Convert.ToInt32(Console.ReadLine());
            carro.QuantidadeDePortas();

            carro.Imprimir();
            Console.ReadKey();
        }

    }
}